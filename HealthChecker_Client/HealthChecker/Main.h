﻿#include "Health.h"

#pragma once
#define MAX_USER_COUNT 100

// Main 대화 상자
#define _WINSOCK_DEPRECATED_NO_WARN


struct UserData
{
	SOCKET h_socket;
	char ip_address[16];
};

class Main : public CDialogEx
{
	DECLARE_DYNAMIC(Main)

public:
	Main(CWnd* pParent = nullptr);   // 표준 생성자입니다.
	virtual ~Main();

private:
	SOCKET mh_socket = INVALID_SOCKET;
	char m_connect_flag = 0; //0:접속해제, 1:접속중, 2:접속됨
	UserData m_user_list[MAX_USER_COUNT];
	

// 대화 상자 데이터입니다.
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_Main };
#endif
	
protected:
	HICON m_hIcon;
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	virtual BOOL OnInitDialog();

	

	DECLARE_MESSAGE_MAP()
public:
	CMonthCalCtrl mainCalendar;
	afx_msg void OnMcnSelchangeMonthcalendar1(NMHDR* pNMHDR, LRESULT* pResult);
	void VisibleTime(CString dateState);
	
	
	afx_msg void OnBnClickedbtnresere();
	int checkAvaliable1[7];
	int checkAvaliable2[7];

	afx_msg void OnBnClickedCancel();
	

	CComboBox mainTimeList;
	afx_msg void OnCbnSelchangeCombotime();
	CComboBox mainTeamCombo;
	CEdit mainNameInput;
	CStatic UITeam;
	CStatic UITime;
	CStatic UIName;
	afx_msg void OnBnClickedSave();
	//CStatic mDate;
	CString getDate;
	CStatic mainDate;
	CMonthCalCtrl mainCal;
	CButton MainBtnSearch;
	CButton MainBtnSave;
	CButton MainBtnModify;

	//Health 구조체를 담을 변수 선언
	Health d23health[7];
	afx_msg void OnBnClickedModify();
//	afx_msg void OnActivate(UINT nState, CWnd* pWndOther, BOOL bMinimized);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct);
protected:
	afx_msg LRESULT On25001(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT On25002(WPARAM wParam, LPARAM lParam);
	void SendFrameData(SOCKET ah_socket, char a_message_id, unsigned short int a_body_size, char* ap_send_data);
public:
	afx_msg void OnDestroy();
	void AddEventString(const char* ap_string);
	CListBox mConnection;
	void SetOtherRev(CString getData);
};
