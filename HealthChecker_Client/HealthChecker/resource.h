﻿//{{NO_DEPENDENCIES}}
// Microsoft Visual C++에서 생성한 포함 파일입니다.
// HealthChecker.rc에서 사용되고 있습니다.
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_HEALTHCHECKER_DIALOG        102
#define IDR_MAINFRAME                   128
#define IDD_Main                        129
#define IDD_Reserve                     132
#define IDC_BUTTON1                     1000
#define IDC_MONTHCALENDAR1              1001
#define IDC_btnSave                     1001
#define IDC_Name                        1002
#define IDC_Time                        1003
#define IDC_Team                        1004
#define IDC_tbName                      1005
#define IDC_cbTeam                      1006
#define IDC_cbTime                      1007
#define IDC_btnCancel                   1008
#define IDC_Date                        1009
#define IDC_btnResere                   1024
#define IDC_COMBOTIME                   1028
#define IDC_mainDate                    1029
#define UI_mainName                     1029
#define IDC_COMBO1                      1030
#define IDC_EDIT1                       1031
#define IDC_mainDate2                   1032
#define UI_mainTime                     1033
#define UI_mainTeam                     1034
#define btn_Modify                      1035
#define btn_Save                        1036
#define IDC_LIST2                       1040
#define connection                      1040

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        134
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1041
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
