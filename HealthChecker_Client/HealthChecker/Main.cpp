﻿// Main.cpp: 구현 파일
//

#include "pch.h"
#include "HealthChecker.h"
#include "Main.h"
#include "afxdialogex.h"
#include "Health.h"

// Main 대화 상자

IMPLEMENT_DYNAMIC(Main, CDialogEx)

Main::Main(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_Main, pParent)
{

}

Main::~Main()
{
}
CString timelist[7] = {
	"10:30 ~ 11:00",
	"11:20 ~ 11:50",
	"13:00 ~ 13:30",
	"14:00 ~ 14:30",
	"15:00 ~ 15:30",
	"16:00 ~ 16:30",
	"17:00 ~ 17:30"
};

CString teamlist[7] = {
	"클라우드 융합팀",
	"아바팀",
	"SITA",
	"OITA",
	"BITA",
	"솔루션 혁신팀",
	"사업 관리팀"
};



void Main::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_COMBOTIME, mainTimeList);
	DDX_Control(pDX, IDC_COMBO1, mainTeamCombo);
	DDX_Control(pDX, IDC_EDIT1, mainNameInput);
	DDX_Control(pDX, UI_mainTeam, UITeam);
	DDX_Control(pDX, UI_mainTime, UITime);
	DDX_Control(pDX, UI_mainName, UIName);
	DDX_Control(pDX, IDC_mainDate2, mainDate);
	DDX_Control(pDX, IDC_MONTHCALENDAR1, mainCal);
	DDX_Control(pDX, IDC_btnResere, MainBtnSearch);
	DDX_Control(pDX, btn_Save, MainBtnSave);
	DDX_Control(pDX, btn_Modify, MainBtnModify);
	DDX_Control(pDX, connection, mConnection);
}

BOOL Main::OnInitDialog()
{
	CDialog::OnInitDialog();

	

	mh_socket = socket(AF_INET, SOCK_STREAM, 0);
	// TODO: 여기에 추가 초기화 작업을 추가합니다.
	struct sockaddr_in srv_addr;
	memset(&srv_addr, 0, sizeof(struct sockaddr_in));
	srv_addr.sin_family = AF_INET;
	srv_addr.sin_addr.s_addr = inet_addr("192.168.0.131");
	srv_addr.sin_port = htons(18000);

	WSAAsyncSelect(mh_socket, m_hWnd, 25001, FD_CONNECT); //서버 미개통시 최대 28초간 발생하는  응답없음을 피하기 위함
	//WSAEventSelect(mh_socket, m_hWnd, 25001);
	m_connect_flag = 1; //서버에 접속시도중...
	AddEventString("서버에 접속을 시도합니다...");
	connect(mh_socket, (LPSOCKADDR)& srv_addr, sizeof(srv_addr));

	// TODO: Add extra initialization here
	mainDate.SetWindowText("원하시는 시간대를 입력해 주세요.");

	for (int i = 0; i < 7; i++)
	{
		mainTimeList.SetCurSel(i);
		mainTimeList.AddString(timelist[i]);
	}
	mainTeamCombo.SetWindowText("팀을 선택해 주세요");

	for (int i = 0; i < 7; i++)
	{
		mainTeamCombo.SetCurSel(i);
		mainTeamCombo.AddString(teamlist[i]);
	}
	return TRUE;
}


BEGIN_MESSAGE_MAP(Main, CDialogEx)
	ON_NOTIFY(MCN_SELCHANGE, IDC_MONTHCALENDAR1, &Main::OnMcnSelchangeMonthcalendar1)
	ON_BN_CLICKED(IDC_btnResere, &Main::OnBnClickedbtnresere)
	ON_BN_CLICKED(IDCANCEL, &Main::OnBnClickedCancel)
	ON_CBN_SELCHANGE(IDC_COMBOTIME, &Main::OnCbnSelchangeCombotime)
	ON_BN_CLICKED(btn_Save, &Main::OnBnClickedSave)
	ON_BN_CLICKED(btn_Modify, &Main::OnBnClickedModify)
//	ON_WM_ACTIVATE()
ON_WM_ERASEBKGND()
ON_WM_DRAWITEM()
ON_WM_CTLCOLOR()
ON_MESSAGE(25001, &Main::On25001)
ON_MESSAGE(25002, &Main::On25002)
ON_WM_DESTROY()
END_MESSAGE_MAP()


// Main 메시지 처리기
//Health health[7];

CString str;

void Main::OnMcnSelchangeMonthcalendar1(NMHDR* pNMHDR, LRESULT* pResult)
{
	CTime tSelected;
	mainCal.GetCurSel(tSelected);
	str.Format(_T("%d년 %d월 %d일"),
		tSelected.GetYear(),
		tSelected.GetMonth(),
		tSelected.GetDay()
	);
	getDate = str;
	//MessageBox(str);
	mainDate.SetWindowText(getDate);

	// 시간 UI 활성화
	(GetDlgItem(UI_mainTime))->ShowWindow(TRUE);
	// 시간 리스트 활성화
	(GetDlgItem(IDC_COMBOTIME))->ShowWindow(TRUE);
	// 시간 조회 버튼 활성화
	(GetDlgItem(IDC_btnResere))->ShowWindow(TRUE);
}

void Main::VisibleTime(CString dateState)
{ 
	int value = 1010;
		
		for (int i = 0; i < 7; i++)
		{
			if (checkAvaliable1[i] == 1)
			{
				(GetDlgItem(value + i))->ShowWindow(FALSE);
			}
			else
			{
				(GetDlgItem(value + i))->ShowWindow(TRUE);
			}

		}	
}

/* 조회 버튼 눌렀을때의 함수 */
void Main::OnBnClickedbtnresere()
{
	MessageBox(d23health[mainTimeList.GetCurSel()].name);
	// TODO: 수정 전
	(GetDlgItem(IDC_COMBO1))->ShowWindow(TRUE);
	(GetDlgItem(IDC_EDIT1))->ShowWindow(TRUE);
	(GetDlgItem(UI_mainTeam))->ShowWindow(TRUE);
	(GetDlgItem(UI_mainTime))->ShowWindow(TRUE);
	(GetDlgItem(UI_mainName))->ShowWindow(TRUE);
	
	(GetDlgItem(btn_Save))->ShowWindow(TRUE);
	(GetDlgItem(btn_Modify))->ShowWindow(TRUE);
	
	int num; //3
	num =mainTimeList.GetCurSel();

	mainNameInput.SetWindowText(d23health[mainTimeList.GetCurSel()].name);
	mainTeamCombo.SetCurSel(d23health[mainTimeList.GetCurSel()].team);
}


void Main::OnBnClickedCancel()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CDialogEx::OnCancel();
}


void Main::OnCbnSelchangeCombotime()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}



/*저장 버튼을 눌렀을때 발생하는 함수*/
void Main::OnBnClickedSave()
{
	CString name;
	CString time;
	CString team;
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	mainNameInput.GetWindowText(name);

	int n; //인덱스
	n = mainTimeList.GetLBTextLen(mainTimeList.GetCurSel());
	mainTimeList.GetLBText(mainTimeList.GetCurSel(), time.GetBuffer(n));
	time.ReleaseBuffer();

	int m;
	m = mainTeamCombo.GetLBTextLen(mainTeamCombo.GetCurSel());
	mainTeamCombo.GetLBText(mainTeamCombo.GetCurSel(), team.GetBuffer(m));
	team.ReleaseBuffer();

	d23health[mainTimeList.GetCurSel()].date = getDate;
	d23health[mainTimeList.GetCurSel()].team = mainTeamCombo.GetCurSel();
	d23health[mainTimeList.GetCurSel()].time = mainTimeList.GetCurSel();
	d23health[mainTimeList.GetCurSel()].name = name;

	//

	CString str;
	CString team2;
	CString time2;

	team2.Format(_T("%d"), mainTimeList.GetCurSel());
	time2.Format(_T("%d"), mainTeamCombo.GetCurSel());
	str = getDate + "," + name + "," + time2 + "," + team2;
	if (m_connect_flag == 2)
	{
		SendFrameData(mh_socket, 1, str.GetLength() + 1, (char*)(const char*)str);
	}

	MessageBox(name + "님 예약이 완료되었습니다");

}

//*@ 수정해야 할때 불러오는 함수
void Main::OnBnClickedModify()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CString name;
	CString time;
	CString team;
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	mainNameInput.GetWindowText(name);

	int n; //인덱스
	n = mainTimeList.GetLBTextLen(mainTimeList.GetCurSel());
	mainTimeList.GetLBText(mainTimeList.GetCurSel(), time.GetBuffer(n));
	time.ReleaseBuffer();

	int m;
	m = mainTeamCombo.GetLBTextLen(mainTeamCombo.GetCurSel());
	mainTeamCombo.GetLBText(mainTeamCombo.GetCurSel(), team.GetBuffer(m));
	team.ReleaseBuffer();

	d23health[mainTimeList.GetCurSel()].date = getDate;
	d23health[mainTimeList.GetCurSel()].team = mainTeamCombo.GetCurSel();
	d23health[mainTimeList.GetCurSel()].time = mainTimeList.GetCurSel();
	d23health[mainTimeList.GetCurSel()].name = name;

	CString str;
	//GetDlgItemText(connection, str);
	str = getDate + "," + name + "," + team + "," + time;
	if (m_connect_flag == 2)
	{
		SendFrameData(mh_socket, 1, str.GetLength() + 1, (char*)(const char*)str);
	}

	MessageBox(name + "님 수정이 완료되었습니다");
}

BOOL Main::OnEraseBkgnd(CDC* pDC)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	CRect rect;
	GetClientRect(&rect);
	CBrush myBrush(RGB(255, 255, 255)); // dialog background color.

	CBrush* pOld = pDC->SelectObject(&myBrush);
	BOOL bRes = pDC->PatBlt(0, 0, rect.Width(), rect.Height(), PATCOPY);
	pDC->SelectObject(pOld); // restore old brush
	return bRes; // CDialog::OnEraseBkgnd(pDC);
}


void Main::OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
		CDC dc;
		RECT rect;
		dc.Attach(lpDrawItemStruct->hDC);   // Get the Button DC to CDC

		rect = lpDrawItemStruct->rcItem;     //Store the Button rect to our local rect.
		dc.Draw3dRect(&rect, RGB(255, 255, 255), RGB(0, 0, 0));
		dc.FillSolidRect(&rect, RGB(31, 31, 31));// 컬러

		UINT state = lpDrawItemStruct->itemState;  //This defines the state of the Push button either pressed or not.
		if ((state & ODS_SELECTED))
			dc.DrawEdge(&rect, EDGE_SUNKEN, BF_RECT);
		else
			dc.DrawEdge(&rect, EDGE_RAISED, BF_RECT);

		dc.SetBkColor(RGB(31, 31, 31));			  //텍스트 백그라운드
		dc.SetTextColor(RGB(255, 255, 255));     //텍스트 컬러

		TCHAR buffer[MAX_PATH];           //To store the Caption of the button.
		ZeroMemory(buffer, MAX_PATH);     //Intializing the buffer to zero
		::GetWindowText(lpDrawItemStruct->hwndItem, buffer, MAX_PATH); //Get the Caption of Button Window

		dc.DrawText(buffer, &rect, DT_CENTER | DT_VCENTER | DT_SINGLELINE);//Redraw the  Caption of Button Window
		dc.Detach();  // Detach the Button DC		
}


afx_msg LRESULT Main::On25001(WPARAM wParam, LPARAM lParam)
{
	if (WSAGETSELECTERROR(lParam))//true:에러발생
	{
		m_connect_flag = 0;
		closesocket(mh_socket);
		mh_socket = INVALID_SOCKET;
		AddEventString("서버에 접속을 실패했습니다.");
	}
	else //false:에러없음
	{
		m_connect_flag = 2;
		WSAAsyncSelect(mh_socket, m_hWnd, 25002, FD_READ | FD_CLOSE);
		//WSAEventSelect(mh_socket, m_hWnd, 25002);
		AddEventString("서버에 접속했습니다.");
	}

	return 0;
}

CString getMessage;

afx_msg LRESULT Main::On25002(WPARAM wParam, LPARAM lParam)
{
	if (WSAGETSELECTEVENT(lParam) == FD_READ)
	{
		if (WSAGETSELECTEVENT(lParam) == FD_READ)
		{
			WSAAsyncSelect(wParam, m_hWnd, 25002, FD_CLOSE);
			//WSAEventSelect(wParam, m_hWnd, 25002);

			char key;
			recv(wParam, &key, 1, 0); //수신버퍼를 1byte로 저장한다.
			if (key == 27)
			{
				char message_id;
				recv(wParam, &message_id, 1, 0);
				unsigned short int body_size;
				recv(wParam, (char*)& body_size, 2, 0);

				char* p_body_data = NULL;
				if (body_size > 0)
				{
					p_body_data = new char[body_size];
					int total = 0, x, retry = 0;
					while (total < body_size)
					{
						x = recv(wParam, p_body_data, body_size, 0);
						if (x == SOCKET_ERROR)break;
						total = total + x;
						if (total < body_size)
						{
							Sleep(50); //ms단위로 처리되는 딜레이 함수
							retry++;
							if (retry > 5) break;
						}
					}
				}

				if (message_id == 1)
				{
					AddEventString(p_body_data);

					/* 수신 받은 메세지 받는 부분*/
					getMessage = p_body_data;
					
				}
				//수신된데이터처리
				if (p_body_data != NULL) delete[] p_body_data;
				WSAAsyncSelect(wParam, m_hWnd, 25002, FD_READ | FD_CLOSE);
				//WSAEventSelect(wParam, m_hWnd, 25002);
			}
		}
		SetOtherRev(getMessage);
	}
	else
	{
		closesocket(wParam);
		CString str;
		for (int i = 0; i < MAX_USER_COUNT; i++)
		{
			if (m_user_list[i].h_socket == wParam)
			{
				m_user_list[i].h_socket = INVALID_SOCKET;
				
				str.Format("사용자가 종료했습니다.:%s", m_user_list[i].ip_address);
				AddEventString(str);
				break;
			}
		}
	}

	return 0;
}

void Main::SetOtherRev(CString getData)
{

	CString name;
	CString date;
	CString team;
	CString time;
	
	CString tempArray[3];
	
	AfxExtractSubString(date, getData, 0, ',');

	AfxExtractSubString(name, getData, 1, ',');

	AfxExtractSubString(team, getData, 2, ',');

	AfxExtractSubString(time, getData, 3, ',');

	int n = _ttoi(time);
	int m = _ttoi(team);
	
	//d23health[n].date = date;
	d23health[n].team = m;
	d23health[n].time = n;
	d23health[n].name = name;

	
}

void Main::SendFrameData(SOCKET ah_socket, char a_message_id, unsigned short int a_body_size, char* ap_send_data)
{
	char* p_send_data = new char[4 + a_body_size]; //보낼 데이터 크기만큼 동적할당
	*p_send_data = 27;
	*(p_send_data + 1) = a_message_id;
	*(unsigned short*)(p_send_data + 2) = a_body_size; //1바이트로 할당된 배열을 캐스팅하여 2바이트까지 사용할수 있도록한다.
	memcpy(p_send_data + 4, ap_send_data, a_body_size); //보낼메시지p_send_data버퍼에 저장

	send(ah_socket, p_send_data, a_body_size + 4, 0); //전송함수

	delete[] p_send_data; //동적할당한 배열 제거
}


void Main::OnDestroy()
{
	CDialogEx::OnDestroy();

	if (mh_socket != INVALID_SOCKET)//소켓이 만들어져 있다면
	{
		closesocket(mh_socket); //소켓을 닫는다
		mh_socket = INVALID_SOCKET; //만약을 위해 보정
	}
}

void Main::AddEventString(const char* ap_string)
{
	while (mConnection.GetCount() > 500)//listbox에 문자열이 500초과시
	{
		mConnection.DeleteString(0);//listbox의 최초 문자열 제거
	}

	int index = mConnection.InsertString(-1, ap_string);
	mConnection.SetCurSel(index);
}
