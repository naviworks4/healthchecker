﻿// Reserve.cpp: 구현 파일
//

#include "pch.h"
#include "HealthChecker.h"
#include "Reserve.h"
#include "afxdialogex.h"
#include "Health.h"
#include "Main.h"


// Reserve 대화 상자

IMPLEMENT_DYNAMIC(Reserve, CDialogEx)

Reserve::Reserve(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_Reserve, pParent)
{
	
}

Reserve::~Reserve()
{
}

void Reserve::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	//  DDX_Control(pDX, IDC_Name, mDate);
	//DDX_Control(pDX, IDC_Date, mDate);
	DDX_Control(pDX, IDC_cbTime, mcbTime);
	DDX_Control(pDX, IDC_cbTeam, mcbTeam);
	DDX_Control(pDX, IDC_tbName, mtbName);
}


BEGIN_MESSAGE_MAP(Reserve, CDialogEx)
	ON_STN_CLICKED(IDC_Name, &Reserve::OnStnClickedName)
	ON_BN_CLICKED(IDC_btnSave, &Reserve::OnBnClickedbtnsave)
	ON_CBN_SELCHANGE(IDC_cbTime, &Reserve::OnCbnSelchangecbtime)
END_MESSAGE_MAP()

//CString timelistr[7] = {
//	L"10:30 ~ 11:00",
//	L"11:20 ~ 11:50",
//	L"13:00 ~ 13:30",
//	L"14:00 ~ 14:30",
//	L"15:00 ~ 15:30",
//	L"16:00 ~ 16:30",
//	L"17:00 ~ 17:30"
//};
//
//CString teamlistr[7] = {
//	L"클라우드 융합팀",
//	L"아바팀",
//	L"SITA",
//	L"OITA",
//	L"BITA",
//	L"솔루션 혁신팀",
//	L"사업 관리팀"
//};
//// Reserve 메시지 처리기
//BOOL Reserve::OnInitDialog()
//{
//    CDialog::OnInitDialog();
//
//    // TODO: Add extra initialization here
//	Health health;
//
//    mDate.SetWindowText(getDate); // Initialize control values
//	mcbTime.SetWindowText(L"예약 시간");
//	for (int i = 0; i < 7; i++)
//	{
//		mcbTime.SetCurSel(i);
//		mcbTime.AddString(timelist[i]);
//	}
//
//	mcbTeam.SetWindowText(L"소속팀");
//	for (int i = 0; i < 7; i++)
//	{
//		mcbTeam.SetCurSel(i);
//		mcbTeam.AddString(teamlist[i]);
//	}
//    return TRUE; // return TRUE unless you set the focus to a control
//    // EXCEPTION: OCX Property Pages should return FALSE
//}

void Reserve::OnStnClickedName()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void Reserve::OnCbnSelchangecbtime()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	//CString date = " ";
	
}
int changeState21[7];
int changeState22[7] = { 0 };

void Reserve::OnBnClickedbtnsave()
{
	Main main;
	CString name;
	mtbName.GetWindowText(name);

	CString time;
	int n; // 인덱스
	n = mcbTime.GetLBTextLen(mcbTime.GetCurSel());
	mcbTime.GetLBText(mcbTime.GetCurSel(), time.GetBuffer(n));
	time.ReleaseBuffer();

	CString team;
	int m;
	m = mcbTeam.GetLBTextLen(mcbTeam.GetCurSel());
	mcbTeam.GetLBText(mcbTeam.GetCurSel(), team.GetBuffer(m));
	team.ReleaseBuffer();
	CString st[7];

	Health health;
	//health.date = getDate;
	health.name = name;
	health.team = mcbTime.GetCurSel();
	health.time= mcbTime.GetCurSel();
	
	
	main.checkAvaliable1[mcbTime.GetCurSel()] = { 1 };
	changeState21[mcbTime.GetCurSel()] = { 1 };
	//main.VisibleTime();
	CDialogEx::OnCancel();
	main.DoModal();


	// TODO : socket 으로 health server로 넘기기
}






