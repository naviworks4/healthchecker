﻿#pragma once


// Reserve 대화 상자

class Reserve : public CDialogEx
{
	DECLARE_DYNAMIC(Reserve)

public:
	Reserve(CWnd* pParent = nullptr);   // 표준 생성자입니다.
	virtual ~Reserve();

// 대화 상자 데이터입니다.
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_Reserve };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	//virtual BOOL OnInitDialog();
	DECLARE_MESSAGE_MAP()
public:

	
	afx_msg void OnStnClickedName();
	afx_msg void OnBnClickedbtnsave();
	CComboBox mcbTime;
	afx_msg void OnCbnSelchangecbtime();
	CComboBox mcbTeam;
	CEdit mtbName;
};
